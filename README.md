# packmlproject - Creador de paquete python


Este es un generador de proyectos de machine learning. Contiene dentro archivos Docker con comandos standarizados, makefiles que incluyen comandos para automatizar entrenamiento a gcp y otros de despliegue de app.
Contiene un trainer.py file para entrenar tu modelo de manera personalizada, se puede modificar a gusto.

`packmlproject` crea un template de paquetes python.

## Instalar `packmlproject`
```bash
pip install git+https://pepeinostroza@bitbucket.org/pepeinostroza/packmlproject.git
```

## Crear un paquete `new_project`

Usar `packmlproject` para crear un nuevo paquete de python:

```bash
packmlproject new_project
```

El árbol del nuevo paquete creado es el siguiente:

```bash
cd new_project
tree
.
├── MANIFEST.in
├── Dockerfile
├── Procfile
├── Makefile
├── README.md
├── new_project
│   ├── __init__.py
│   ├── data
│   ├── data.py
│   ├── encoders.py
│   ├── gcp.py
│   ├── trainer.py
│   └── utils.py
├── api
│   ├── __init__.py
│   ├── app.py
│   ├── app_streamlit.py
│   └── fast.py
├── notebooks
├── raw_data
├── requirements.txt
├── scripts
│   └── new_project-run
├── setup.py
├── setup.sh
└── tests
    ├── __init__.py
    └── data_test.py

6 directories, 21 files
```
